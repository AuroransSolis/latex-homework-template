\documentclass{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bookmark}
\usepackage{caption}
\usepackage[siunitx]{circuitikz}
\usepackage{expkv-def}
\usepackage{extramarks}
\usepackage{fancyhdr}
\usepackage{gnuplot-lua-tikz}
\usepackage{gnuplottex}
\usepackage{graphicx}
\usepackage{iftex}
\usepackage{karnaugh-map}
\usepackage{mathalpha}
\usepackage{mathtools}
\usepackage{minted}
\usepackage{pdftexcmds}
\usepackage{siunitx}
\usepackage{steinmetz}
\usepackage{subcaption}
\usepackage{tikz}

% Make sure to have PdfLaTeX read Unicode - LuaLaTeX and XeLaTeX don't need this to be set manually.
\ifpdftex
    \usepackage[utf8x]{inputenc}
\fi

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass: \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}
\setcounter{secnumdepth}{0}

\AtBeginDocument{
	\hypersetup{
		pdftitle = {\hmwkClass: \hmwkTitle},
		pdfauthor = {\hmwkAuthorName}
	}
}

\hypersetup{
	colorlinks = true,
	linkcolor = blue,
	citecolor = blue,
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework Title}
\newcommand{\hmwkDueDate}{Homework due date}
\newcommand{\hmwkClass}{Class}
\newcommand{\hmwkClassInstructor}{Professor Firstname Lastname}
\newcommand{\hmwkAuthorName}{\textbf{Your Name}}


%
% Title Page
%

\title{
	\vspace{2in}
	\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
	\vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
	\vspace{3in}
	\author{\hmwkAuthorName}
	\date{\hmwkDueDate}
}


%
% Homework Problem Environment
%
% This takes three options:
%     1. problemNumber
%     2. chapterNumber
%     3. showChapterNumber
%
% The names are self-explanatory. If a problem number is provided, then that problem number will be
% used. Otherwise, the number after the previous problem number will be used. If a chapter number is
% provided, then that chapter number will be used. Otherwise, the previous chapter number will be
% used. If showChapterNumber is present in the options, then the problem number format will be
% "Problem chapterNumber.problemNumber". Otherwise, the problem number format will be
% "Problem problemNumber".
%

\newcounter{partCounter}
\setcounter{partCounter}{1}
\newcounter{problemCounter}
\setcounter{problemCounter}{1}
\newcounter{chapterCounter}
\setcounter{chapterCounter}{1}
\newcounter{storedChapterNumber}
\setcounter{storedChapterNumber}{1}
\ekvdefinekeys{homeworkProblemOpts}{
	int problemNumber = \problemNumber,
	initial problemNumber = -1,
	int chapterNumber = \chapterNumber,
	initial chapterNumber = -1,
	bool showChapterNumber = \ifShowChapterNumber,
	initial showChapterNumber = false,
}
\newenvironment{homeworkProblem}[1][]{
	% Set option values from arguments.
	\ekvset{homeworkProblemOpts}{#1}
	% If the problem number is greater than 0, then set the problem counter to that. Otherwise set
	% the problem counter to a default of 1.
	\ifnum\problemNumber>-1
		\setcounter{problemCounter}{\problemNumber}
	\fi
	\ifnum\chapterNumber>-1
		\setcounter{chapterCounter}{\chapterNumber}
	\fi
	\setcounter{partCounter}{0}
	\ifShowChapterNumber
		\ifdefined\showProblem
			\renewcommand{\showProblem}[0]{Problem \arabic{chapterCounter}.\arabic{problemCounter}}
		\else
			\newcommand{\showProblem}[0]{Problem \arabic{chapterCounter}.\arabic{problemCounter}}
		\fi
	\else
		\ifdefined\showProblem
			\renewcommand{\showProblem}[0]{Problem \arabic{problemCounter}}
		\else
			\newcommand{\showProblem}[0]{Problem \arabic{problemCounter}}
		\fi
	\fi
	\nobreak\extramarks{\showProblem}{}\nobreak{}
	\section{\showProblem}
	\nobreak\extramarks{}{\showProblem{} continued on next page\ldots}\nobreak{}
	\nobreak\extramarks{%
		\showProblem{} (continued)%
	}{%
		\showProblem{} continued on next page\ldots%
	}\nobreak{}
}{
	\nobreak\extramarks{\showProblem{} (continued)}{}\nobreak{}
	\stepcounter{problemCounter}
	\nobreak\extramarks{}{}\nobreak{}
}

%
% Homework part command
%
% This takes two options:
%     1. partNumber
%     2. showPartAs
%
% partNumber is the number (or number representing the letter) to start at. This is -1 by default,
% which then pulls from the partNumber counter to display the part number/letter. showPartAs is a
% choice option between "alpha" and "numer" (default of "alpha") which map to \Alph and \arabic
% respectively, and are used to show the partNumber counter as a letter or number respectively.
%
\newcommand{\displayPartCounter}[0]{\Alph{partCounter}}
\newcommand{\displayPart}[0]{\displayPartCounter{}}
\ekvdefinekeys{homeworkPartOpts}{
	int partNumber = \partNumber,
	initial partNumber = -1,
	choice showPartAs = {
		alpha = \renewcommand{\displayPartCounter}[0]{\alph{partCounter}},
		Alpha = \renewcommand{\displayPartCounter}[0]{\Alph{partCounter}},
		numer = \renewcommand{\displayPartCounter}[0]{\arabic{partCounter}},
		roman = \renewcommand{\displayPartCounter}[0]{\roman{partCounter}},
		Roman = \renewcommand{\displayPartCounter}[0]{\Roman{partCounter}},
	},
	initial showPartAs = Alpha,
	choice extra = {
		none = \renewcommand{\displayPart}[0]{\displayPartCounter{}},
		trailingStop = \renewcommand{\displayPart}[0]{\displayPartCounter{}.},
		parens = \renewcommand{\displayPart}[0]{(\displayPartCounter{})},
		rparens = \renewcommand{\displayPart}[0]{\displayPartCounter{})},
	},
	initial extra = none,
}
\newcommand{\homeworkPart}[1][]{
	% Set options from arguments.
	\ekvset{homeworkPartOpts}{
		showPartAs = Alpha,
		extra = none,
		#1
	}
	\ifnum\partNumber>0
		\setcounter{partCounter}{\partNumber}
	\else
		\stepcounter{partCounter}
	\fi
	\subsection{Part \displayPart{}}
}

\newcommand{\nextChapter}{\stepcounter{chapterCounter}\setcounter{problemCounter}{1}}
\newcommand{\setChapter}[1]{\setcounter{chapterCounter}{#1}\setcounter{problemCounter}{1}}


%
% Package configuration
%

% amsmath
\allowdisplaybreaks[1]
\DeclareMathOperator{\sgn}{sgn}

% circuitikz
\ctikzset{
	american,
	capacitors/thickness = 3,
	capacitors/width = 0.1,
	resistors/scale = 0.6,
	resistors/thickness = 4,
	tripoles/mos style/arrows,
	tripoles/pmos style/emptycircle,
}

% gnuplottex
% For some reason on my computers, gnuplottex really doesn't like running the gnuplot commands. This
% was necessary to get it to work properly.
\makeatletter
\ifluatex
    \ifnum\pdf@shellescape=\@ne
        \ShellEscapetrue
    \fi
\fi
\makeatother

% siunitx
\sisetup{exponent-product = \cdot, per-mode = fraction}

% tikz
\usetikzlibrary{arrows, automata, calc, matrix, positioning, through}


%
% Various Helper Commands
%
% I took most of these from from the homework I've been doing this semester. Prune these or add to
% it as you see fit. They're mostly signals/power systems related so I dunno how much use you - the
% alien person unknown to me using my template - will get out of these, but they've been helpful to
% me.
%

% Units and siunitx helper commands
\DeclareSIUnit{\hp}{\text{hp}}
\DeclareSIUnit{\va}{\text{VA}}
\DeclareSIUnit{\var}{\text{VAr}}
\DeclareSIQualifier{\rms}{rms}
\ifdefined\qty
\else
	\newcommand{\qty}[2]{#1\si{#2}}
\fi
\newcommand{\phasor}[2]{#1\phase{#2}}
\newcommand{\volts}[2][]{\qty{#2}{#1\volt}}
\newcommand{\amps}[2][]{\qty{#2}{#1\ampere}}
\newcommand{\ohms}[2][]{\qty{#2}{#1\ohm}}
\newcommand{\freq}[2][]{\qty{#2}{#1\hertz}}
\newcommand{\henrys}[2][]{\qty{#2}{#1\henry}}
\newcommand{\farads}[2][]{\qty{#2}{#1\farad}}
\newcommand{\vas}[2][]{\qty{#2}{#1\va}}
\newcommand{\vars}[2][]{\qty{#2}{#1\var}}
\newcommand{\watts}[2][]{\qty{#2}{#1\watt}}
\newcommand{\secs}[2][]{\qty{#2}{#1\second}}
\newcommand{\pct}[1]{\qty{#1}{\percent}}
\newcommand{\db}[2][]{\qty{#2}{#1\decibel}}
\newcommand{\dec}[2][]{\qty{#2}{#1\decade}}
\newcommand{\metres}[2][]{\qty{#2}{#1\metre}}
\newcommand{\hps}[2][]{\qty{#2}{#1\hp}}
\newcommand{\vrms}[2][]{\qty{#2}{#1\volt\rms}}
\newcommand{\arms}[2][]{\qty{#2}{#1\ampere\rms}}
\newcommand{\imp}[1]{\complexnum[output-complex-root = j]{#1}\unit{\ohm}}

% Trig shorthands
\newcommand{\sincont}[1]{\sin\parens{#1}}
\newcommand{\coscont}[1]{\cos\parens{#1}}
\newcommand{\tancont}[1]{\tan\parens{#1}}
\newcommand{\sindisc}[1]{\sin\squareb{#1}}
\newcommand{\cosdisc}[1]{\cos\squareb{#1}}
\newcommand{\tandisc}[1]{\tan\squareb{#1}}
\newcommand{\asin}[1]{\sin^{-1}\parens{#1}}
\newcommand{\acos}[1]{\cos^{-1}\parens{#1}}
\newcommand{\atan}[1]{\tan^{-1}\parens{#1}}

% Transform shorthands
\newcommand{\lt}[1]{\mathcal{L}\curlyb{#1}}
\newcommand{\ilt}[1]{\mathcal{L}^{-1}\curlyb{#1}}
\newcommand{\ft}[1]{\mathcal{F}\curlyb{#1}}
\newcommand{\ift}[1]{\mathcal{F}^{-1}\curlyb{#1}}

% Useful shorthands for transforms and RoC bounds
\newcommand{\re}[1]{\mathfrak{Re}\curlyb{#1}}
\newcommand{\im}[1]{\mathfrak{Im}\curlyb{#1}}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\stepcont}[1]{\mu\parens{#1}}
\newcommand{\stepdisc}[1]{\mu\squareb{#1}}
\newcommand{\pulsecont}[1]{\delta\parens{#1}}
\newcommand{\pulsedisc}[1]{\delta\squareb{#1}}

% Misc
\newcommand{\bsym}[1]{\boldsymbol{#1}}
\newcommand{\maxinline}[1]{\mintinline{maxima}{#1}}
\newcommand{\svinline}[1]{\mintinline{sv}{#1}}
\newcommand{\parens}[1]{\left(#1\right)}
\newcommand{\squareb}[1]{\left[#1\right]}
\newcommand{\curlyb}[1]{\left\{#1\right\}}
\newcommand{\angleb}[1]{\left<#1\right>}
\newcommand{\fn}[2]{#1\parens{#2}}
\newcommand{\sign}[1]{\sgn\parens{#1}}
\newcommand{\ra}[0]{\rightarrow}
\newcommand{\Ra}[0]{\Rightarrow}

\begin{document}
	\pagenumbering{gobble}

	\maketitle

	\pagebreak

	\pagenumbering{arabic}

	% Your stuff here.
\end{document}
