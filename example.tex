\documentclass{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{bookmark}
\usepackage{caption}
\usepackage[siunitx]{circuitikz}
\usepackage{expkv-def}
\usepackage{extramarks}
\usepackage{fancyhdr}
\usepackage{gnuplot-lua-tikz}
\usepackage{gnuplottex}
\usepackage{graphicx}
\usepackage{iftex}
\usepackage{karnaugh-map}
\usepackage{mathalpha}
\usepackage{mathtools}
\usepackage{minted}
\usepackage{pdftexcmds}
\usepackage{siunitx}
\usepackage{steinmetz}
\usepackage{subcaption}
\usepackage{tikz}

% Make sure to have PdfLaTeX read Unicode - LuaLaTeX and XeLaTeX don't need this to be set manually.
\ifpdftex
	\usepackage[utf8x]{inputenc}
\fi

%
% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass: \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}
\setcounter{secnumdepth}{0}

\AtBeginDocument{
	\hypersetup{
		pdftitle = {\hmwkClass: \hmwkTitle},
		pdfauthor = {\hmwkAuthorName}
	}
}

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Homework Title}
\newcommand{\hmwkDueDate}{Homework due date}
\newcommand{\hmwkClass}{Class}
\newcommand{\hmwkClassInstructor}{Professor Firstname Lastname}
\newcommand{\hmwkAuthorName}{\textbf{Your Name}}


%
% Title Page
%

\title{
	\vspace{2in}
	\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
	\vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
	\vspace{3in}
	\author{\hmwkAuthorName}
	\date{\hmwkDueDate}
}


%
% Homework Problem Environment
%
% This takes three options:
%     1. problemNumber
%     2. chapterNumber
%     3. showChapterNumber
%
% The names are self-explanatory. If a problem number is provided, then that problem number will be
% used. Otherwise, the number after the previous problem number will be used. If a chapter number is
% provided, then that chapter number will be used. Otherwise, the previous chapter number will be
% used. If showChapterNumber is present in the options, then the problem number format will be
% "Problem chapterNumber.problemNumber". Otherwise, the problem number format will be
% "Problem problemNumber".
%

\newcounter{partCounter}
\setcounter{partCounter}{1}
\newcounter{problemCounter}
\setcounter{problemCounter}{1}
\newcounter{chapterCounter}
\setcounter{chapterCounter}{1}
\newcounter{storedChapterNumber}
\setcounter{storedChapterNumber}{1}
\ekvdefinekeys{homeworkProblemOpts}{
	int problemNumber = \problemNumber,
	initial problemNumber = -1,
	int chapterNumber = \chapterNumber,
	initial chapterNumber = -1,
	bool showChapterNumber = \ifShowChapterNumber,
}
\newenvironment{homeworkProblem}[1][]{
	% Set option values from arguments.
	\ekvset{homeworkProblemOpts}{#1}
	% If the problem number is greater than 0, then set the problem counter to that. Otherwise set
	% the problem counter to a default of 1.
	\ifnum\problemNumber>-1
		\setcounter{problemCounter}{\problemNumber}
	\fi
	\ifnum\chapterNumber>-1
		\setcounter{chapterCounter}{\chapterNumber}
	\fi
	\setcounter{partCounter}{0}
	\ifShowChapterNumber
		\ifdefined\showProblem
			\renewcommand{\showProblem}[0]{Problem \arabic{chapterCounter}.\arabic{problemCounter}}
		\else
			\newcommand{\showProblem}[0]{Problem \arabic{chapterCounter}.\arabic{problemCounter}}
		\fi
	\else
		\ifdefined\showProblem
			\renewcommand{\showProblem}[0]{Problem \arabic{problemCounter}}
		\else
			\newcommand{\showProblem}[0]{Problem \arabic{problemCounter}}
		\fi
	\fi
	\nobreak\extramarks{\showProblem}{}\nobreak{}
	\section{\showProblem}
	\nobreak\extramarks{}{\showProblem{} continued on next page\ldots}\nobreak{}
	\nobreak\extramarks{
		\showProblem{} (continued)
	}{
		\showProblem{} continued on next page\ldots
	}\nobreak{}
}{
	\nobreak\extramarks{\showProblem{} (continued)}{}\nobreak{}
	\stepcounter{problemCounter}
	\nobreak\extramarks{}{}\nobreak{}
}

%
% Homework part command
%
% This takes two options:
%     1. partNumber
%     2. showPartAs
%
% partNumber is the number (or number representing the letter) to start at. This is -1 by default,
% which then pulls from the partNumber counter to display the part number/letter. showPartAs is a
% choice option between "alpha" and "numer" (default of "alpha") which map to \Alph and \arabic
% respectively, and are used to show the partNumber counter as a letter or number respectively.
%
\newcommand{\displayPartCounter}[0]{\Alph{partCounter}}
\ekvdefinekeys{homeworkPartOpts}{
	int partNumber = \partNumber,
	initial partNumber = -1,
	choice showPartAs = {
		alpha = \renewcommand{\displayPartCounter}[0]{\Alph{partCounter}},
		numer = \renewcommand{\displayPartCounter}[0]{\arabic{partCounter}},
	},
}
\newcommand{\homeworkPart}[1][]{
	% Set options from arguments.
	\ekvset{homeworkPartOpts}{showPartAs = alpha, #1}
	\ifnum\partNumber>0
		\setcounter{partCounter}{\partNumber}
	\else
		\stepcounter{partCounter}
	\fi
	\subsection{Part \displayPartCounter{}}
}

\newcommand{\nextChapter}{\stepcounter{chapterCounter}\setcounter{problemCounter}{1}}
\newcommand{\setChapter}[1]{\setcounter{chapterCounter}{#1}\setcounter{problemCounter}{1}}


%
% Package configuration
%

% amsmath
\allowdisplaybreaks[1]
\DeclareMathOperator{\sgn}{sgn}

% circuitikz
\ctikzset{
	american,
	capacitors/thickness = 3,
	capacitors/width = 0.1,
	resistors/scale = 0.6,
	resistors/thickness = 4,
	tripoles/mos style/arrows,
	tripoles/pmos style/emptycircle,
}

% gnuplottex
% For some reason on my computers, gnuplottex really doesn't like running the gnuplot commands. This
% was necessary to get it to work properly.
\makeatletter
\ifluatex
	\ifnum\pdf@shellescape=\@ne
		\ShellEscapetrue
	\fi
\fi
\makeatother

% siunitx
\sisetup{exponent-product = \cdot, per-mode = fraction}

% tikz
\usetikzlibrary{arrows, automata, calc, matrix, positioning, through}

%
% Various Helper Commands
% 
% I took most of these from from the homework I've been doing this semester. Prune these or add to
% it as you see fit. They're mostly signals/power systems related so I dunno how much use you - the
% alien person unknown to me using my template - will get out of these, but they've been helpful to
% me.
%

% Units and siunitx helper commands
\DeclareSIUnit{\hp}{\text{hp}}
\DeclareSIUnit{\va}{\text{VA}}
\DeclareSIUnit{\var}{\text{VAr}}
\DeclareSIQualifier{\rms}{rms}
\ifdefined\qty
\else
	\newcommand{\qty}[2]{#1\si{#2}}
\fi
\newcommand{\phasor}[2]{#1\phase{#2}}
\newcommand{\volts}[2][]{\qty{#2}{#1\volt}}
\newcommand{\amps}[2][]{\qty{#2}{#1\ampere}}
\newcommand{\ohms}[2][]{\qty{#2}{#1\ohm}}
\newcommand{\freq}[2][]{\qty{#2}{#1\hertz}}
\newcommand{\henrys}[2][]{\qty{#2}{#1\henry}}
\newcommand{\farads}[2][]{\qty{#2}{#1\farad}}
\newcommand{\vas}[2][]{\qty{#2}{#1\va}}
\newcommand{\vars}[2][]{\qty{#2}{#1\var}}
\newcommand{\watts}[2][]{\qty{#2}{#1\watt}}
\newcommand{\secs}[2][]{\qty{#2}{#1\second}}
\newcommand{\pct}[1]{\qty{#1}{\percent}}
\newcommand{\db}[2][]{\qty{#2}{#1\decibel}}
\newcommand{\dec}[2][]{\qty{#2}{#1\decade}}
\newcommand{\metres}[2][]{\qty{#2}{#1\metre}}
\newcommand{\hps}[2][]{\qty{#2}{#1\hp}}
\newcommand{\vrms}[2][]{\qty{#2}{#1\volt\rms}}
\newcommand{\arms}[2][]{\qty{#2}{#1\ampere\rms}}
\newcommand{\imp}[1]{\complexnum[output-complex-root = j]{#1}\unit{\ohm}}

% Trig shorthands
\newcommand{\sincont}[1]{\sin\parens{#1}}
\newcommand{\coscont}[1]{\cos\parens{#1}}
\newcommand{\tancont}[1]{\tan\parens{#1}}
\newcommand{\sindisc}[1]{\sin\squareb{#1}}
\newcommand{\cosdisc}[1]{\cos\squareb{#1}}
\newcommand{\tandisc}[1]{\tan\squareb{#1}}
\newcommand{\asin}[1]{\sin^{-1}\parens{#1}}
\newcommand{\acos}[1]{\cos^{-1}\parens{#1}}
\newcommand{\atan}[1]{\tan^{-1}\parens{#1}}

% Transform shorthands
\newcommand{\lt}[1]{\mathcal{L}\curlyb{#1}}
\newcommand{\ilt}[1]{\mathcal{L}^{-1}\curlyb{#1}}
\newcommand{\ft}[1]{\mathcal{F}\curlyb{#1}}
\newcommand{\ift}[1]{\mathcal{F}^{-1}\curlyb{#1}}

% Useful shorthands for transforms and RoC bounds
\newcommand{\re}[1]{\mathfrak{Re}\curlyb{#1}}
\newcommand{\im}[1]{\mathfrak{Im}\curlyb{#1}}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\stepcont}[1]{\mu\parens{#1}}
\newcommand{\stepdisc}[1]{\mu\squareb{#1}}
\newcommand{\pulsecont}[1]{\delta\parens{#1}}
\newcommand{\pulsedisc}[1]{\delta\squareb{#1}}

% Misc
\newcommand{\bsym}[1]{\boldsymbol{#1}}
\newcommand{\maxinline}[1]{\mintinline{maxima}{#1}}
\newcommand{\svinline}[1]{\mintinline{sv}{#1}}
\newcommand{\parens}[1]{\left(#1\right)}
\newcommand{\squareb}[1]{\left[#1\right]}
\newcommand{\curlyb}[1]{\left\{#1\right\}}
\newcommand{\angleb}[1]{\left<#1\right>}
\newcommand{\fn}[2]{#1\parens{#2}}
\newcommand{\sign}[1]{\sgn\parens{#1}}
\newcommand{\ra}[0]{\rightarrow}
\newcommand{\Ra}[0]{\Rightarrow}


\begin{document}
	\maketitle

	\pagebreak

	\begin{homeworkProblem}
		\homeworkPart
		Yes, it is possible for valid Verilog code to be unsynthesisable. \\
		
		\homeworkPart
		Unless otherwise defined, the default nettype is \svinline{wire}. \\
		
		\homeworkPart
		The true statement is the first one. SystemVerilog is a superset of Verilog, and so any valid
		Verilog is valid SystemVerilog. \\
		
		\homeworkPart
		A \svinline{parameter} can be given a value from parent modules, but a \svinline{localparam} is
		more or less an immutable variable whose value is provided by a constant (compile-time
		evaluable) expression. \\
		
		\homeworkPart
		\begin{minted}[linenos, autogobble, tabsize = 4]{sv}
			module Hw12Q1PE(input a, input b, input c, input d, output logic f);
				wire minterm_0;
				and a0 (minterm_0, a, b);
				wire minterm_1;
				and a1 (minterm_1, c, d);
				or o0 (f, minterm_0, minterm_1);
			endmodule
		\end{minted}
		
		\homeworkPart[showPartAs = numer]
		You defined abstraction as "...a view of something in which some information is hidden." \\
		
		\homeworkPart
		The provided equation is $F(A, B, C, D) = \Sigma m(3, 4, 7, 9, 13, 15) + \Sigma d(5, 14)$.
		\begin{figure}[h]
			\centering
			\begin{karnaugh-map}[4][4][1][$CD$][$AB$]
				\maxterms{0, 1, 2, 6, 8, 10, 11, 12};
				\minterms{3, 4, 7, 9, 13, 15};
				\terms{5, 14}{X};
				\implicant{3}{7};
				\implicant{4}{5};
				\implicant{15}{14};
				\implicant{13}{9};
			\end{karnaugh-map}
		\end{figure} \\
		The SOP is then $\bar{A}CD+\bar{A}B\bar{C}+ABC+A\bar{C}D$. \\
		
		\homeworkPart
		For a SR flip-flop, $Q^* = S + Q\bar{R}$. \\
		
		\homeworkPart
		For a JK flip-flop, $Q^* = \bar{Q}J + Q\bar{K}$. \\
		
		\homeworkPart
		\begin{minted}[linenos, autogobble, tabsize = 4]{sv}
			module JkFlipFlop(input clk, input j, input k, output logic q);
				always_ff @ (posedge clk) begin
					q <= (~q & j) | (q & ~k);
				end
			endmodule
		\end{minted}
	\end{homeworkProblem}

	\pagebreak

	\begin{homeworkProblem}
		Make a finite state machine that detects the sequence 101 on a serial one-bit input $X$. It
		should have a one-bit output $Z$ that is 0 unless 101 has been detected. The machine should
		have a reset that takes it to an initial state. Assume a Moore model.
		\homeworkPart
		\begin{tikzpicture}
			[
				->,
				> = stealth',
				node distance = 2cm,
			]
			\node[state with output, initial] (s0) {$S_0$ \nodepart{lower} $0$};
			\node[state with output, right of=s0] (s1) {$S_1$ \nodepart{lower} $0$};
			\node[state with output, right of=s1] (s2) {$S_2$ \nodepart{lower} $0$};
			\node[state with output, right of=s2] (s3) {$S_3$ \nodepart{lower} $1$};
			\path[->]
				(s0)
					edge[loop above] node{0} (s0)
					edge[above] node{1} (s1)
				(s1)
					edge[loop above] node{1} (s1)
					edge[above] node{0} (s2)
				(s2)
					edge [bend left=40, below] node{0} (s0)
					edge [above] node{1} (s3)
				(s3)
					edge[bend left, below] node{0} (s2)
					edge[bend right, above] node{1} (s1);
		\end{tikzpicture}
		
		\homeworkPart
		Four states requires two flip flops, $Q_0$ and $Q_1$. Thus the state assignment table could be
		\begin{table}[h!]
			\begin{tabular}{c|c c}
				$\bsym{S_n}$ & $\bsym{Q_1}$ & $\bsym{Q_0}$ \\
				\hline
				0 & 0 & 0 \\
				1 & 0 & 1 \\
				2 & 1 & 0 \\
				3 & 1 & 1
			\end{tabular}
		\end{table} \\

		\homeworkPart
		Then the state transition table might look like
		\begin{table}[h!]
			\begin{tabular}{c c c|c c c}
				$\bsym{X}$ & $\bsym{Q_1}$ & $\bsym{Q_0}$ & $\bsym{Q_1^*}$ & $\bsym{Q_0^*}$ & $\bsym{Z}$ \\
				\hline
				0 & 0 & 0 & 0 & 0 & 0 \\
				1 & 0 & 0 & 0 & 1 & 0 \\
				0 & 0 & 1 & 1 & 0 & 0 \\
				1 & 0 & 1 & 0 & 1 & 0 \\
				0 & 1 & 0 & 0 & 0 & 0 \\
				1 & 1 & 0 & 1 & 1 & 0 \\
				0 & 1 & 1 & 1 & 0 & 1 \\
				1 & 1 & 1 & 0 & 1 & 1
			\end{tabular}
		\end{table}
	\end{homeworkProblem}

	\pagebreak

	\begin{homeworkProblem}[chapterNumber = 5, problemNumber = 11, showChapterNumber = true]
		A \emph{p}-channel MOSFET with a threshold voltage $V_{tp} = \qty{-0.7}{\volt}$ has its
		source connected to ground.
		
		\homeworkPart
		What should the gate voltage be for the device to operate with an overdrive voltage of
		$\abs{V_{ov}} = \volts{0.4}$? \\
		
		The circuit described in the problem is as follows:
		\begin{figure}[h]
			\centering
			\begin{circuitikz}[american]
				\node [pmos, arrowmos](pmos){};
				\draw (pmos.S) node[tlground, rotate = 180]{};
				\draw (pmos.G) to[short]
				++(-0.1, 0) node[ocirc]{};
				\draw (pmos.D) to[short] ++(0, -0.1) node[ocirc]{};
			\end{circuitikz}
		\end{figure} \\
		Provided that the open circuits lead to other elements that provide the conditions described
		in this and other problems. On to the work for this part, though. \\
		
		The relationship between $v_{OV}$, $v_{SG}$, and $V_{tp}$ for a PMOS transistor in saturation
		is $v_{SG} = \abs{v_{OV}} + \abs{V_{tp}}$. Plugging our values into this, we get
		\begin{align*}
			v_{SG} &= \abs{v_{OV}} + \abs{V_{tp}} \\
				&= \volts{0.4} + \abs{\qty{-0.7}{\volt}} \\
				&= \volts{0.4} + \volts{0.7} \\
				&= \volts{1.1}
		\end{align*}
		Then we have that $v_{SG} = v_S - v_G$, but since the source is grounded, $v_S = 0$, so then
		\begin{align*}
			v_{SG} &= 0 - v_G \\
			\volts{1.1} &= -v_G \\
			\qty{-1.1}{\volt} &= v_G
		\end{align*}
		Thus a gate voltage of \qty{-1.1}{\volt} is necessary for the device to operate with an overdrive voltage of $\abs{v_{OV}} = \volts{0.4}$.
		
		\homeworkPart
		With the gate voltage as in (a), what is the highest voltage allowed at the drain while the
		device operates in the saturation region? \\
		
		For a PMOS device to be in saturation, both $v_{SG} > \abs{V_{tp}}$ and $v_{SD} \ge
		\abs{v_{OV}}$ must be true. From the previous part we know that $v_{SG} > \abs{V_{tp}}$ is
		true, so let's find the limit for the other condition.
		\begin{align*}
			v_{SD} &\geq v_{OV} \\
			v_S - v_D &\geq \volts{0.4} \\
			-v_D &\geq \volts{0.4} \\
			v_D &\leq \qty{-0.4}{\volt}
		\end{align*}
		And so the highest drain voltage possible while in saturation is \qty{-0.4}{\volt}.
		
		\homeworkPart
		If the drain current obtained in (b) is \qty{0.5}{\milli\ampere}, what would the current be
		for $v_D = \qty{-20}{\milli\volt}$ and for $v_D = \qty{-2}{\volt}$? \\
		
		First, we need to determine what $k_p = k_p'\frac{W}{L}$ is. Given that the device is in
		saturation in (b), we use the saturation current equation:
		\begin{align*}
			i_D &= \frac{1}{2}k_pv_{OV}^2 \\
			\qty{0.5}{\milli\ampere} &= \frac{1}{2}k_p\qty{0.16}{\volt\squared} \\
			\frac{0.001}{0.16}\si{\ampere\per\volt\squared} &= k_p \\
			k_p &= 6.25\cdot10^{-3}\si{\ampere\per\volt\squared}
		\end{align*}
		
		If $v_D = \qty{-20}{\milli\volt}$, then
		\begin{align*}
			v_{SD} &\gtreqless \abs{v_{OV}} \\
			v_S - v_D &\gtreqless \volts{0.4} \\
			\qty{-20}{\milli\volt} &< \volts{0.4}
		\end{align*}
		The device is in triode, so we use the triode region equation:
		\begin{align*}
			i_D &= k_p\left(\abs{v_{OV}} - \frac{1}{2}v_{SD}\right)v_{SD} \\
				&= 6.25\cdot10^{-3}\si{\ampere\per\volt\squared}\left(
					\volts{0.4} - \frac{1}{2}\left(0 + \qty{20}{\milli\volt}\right)
				\right)\left(0 + \qty{20}{\milli\volt}\right) \\
				&= 6.25\cdot10^{-3}\si{\ampere\per\volt\squared}
					\left(\volts{0.4} - \volts{0.01}\right)
					\volts{0.02} \\
				&= 6.25\cdot10^{-3}\si{\ampere\per\volt\squared}
					\cdot\volts{0.39}
					\cdot\volts{0.02} \\
				&= \qty{48.75}{\micro\ampere}
		\end{align*}
		If $v_D = \qty{-2}{\volt}$, then the device is in saturation ($-2 < -0.4$). Device current
		in saturation is unaffected by drain voltage, so $i_D = \qty{0.5}{\milli\ampere}$.
	\end{homeworkProblem}

	\pagebreak

	\begin{homeworkProblem}
		The electrical network shown in Figure 3.43 has a voltage source of 140V and the value of the
		impedances is as follows: $Z_1 = \imp{5 - 8j}$, $Z_2 = \imp{10 + 5j}$, $Z_3 = \imp{5 - 5j}$, and
		$Z_4 = \imp{15 + 10j}$. \\
		\includegraphics*{figure-3.43.png}
		
		First, the total impedance of the circuit was determined using the following Maxima code:
		\begin{minted}[autogobble, linenos, tabsize = 4]{maxima}
			series(inputs) := rectform(ratsimp(sum(inputs[i], i, 1, length(inputs))))$
			parallel(inputs) := rectform(ratsimp(sum(inputs[i]^(-1), i, 1, length(inputs))^(-1)))$
			Z_1: 5 - 8 * %i$
			Z_2: 10 + 5 * %i$
			Z_3: 5 - 5 * %i$
			Z_4: 15 + 10 * %i$
			Z_34: parallel([Z_3, Z_4]);
			Z_234: series([Z_2, Z_34]);
			Z_1234: parallel([Z_1, Z_234]);
		\end{minted}
		Where $\text{\maxinline{Z_34}} = Z_3 | Z_4$, $\text{\maxinline{Z_234}} = Z_2 +
		\text{\maxinline{Z_34}} = Z_2 + (Z_3 | Z_4)$, and $\text{\maxinline{Z_1234}} = Z_4 |
		\text{\maxinline{Z_234}} = Z_4 | (Z_2 + \text{\maxinline{Z_34}}) = Z_1 | (Z_2 + (Z_3 | Z_4))$,
		using $+$ for series notation and $|$ for parallel notation. The output of the program provided
		the following:
		\begin{align*}
			\text{\maxinline{Z_34}} &= \frac{95}{17} - \frac{45j}{17} \\
			\text{\maxinline{Z_234}} &= \frac{265}{17} + \frac{40j}{17} \\
			\text{\maxinline{Z_1234}} &= \frac{22355}{3874} - \frac{7560j}{1937}
		\end{align*}
		From this, we can find that the complex current for the circuit is
		\begin{align*}
			\frac{\qty{240}{\volt}}{\text{\maxinline{Z_1234}}\si{\ohm}}
				&= \frac{\qty{240}{\volt}}{\frac{22355}{3874} - \frac{7560j}{1937}} \\
				&= \frac{429216}{15041} + \frac{290304j}{15041}
		\end{align*}
		Furthermore, the current through each of the impedances can be determined using the following
		Maxima code (in the same session or file as the above code):
		\begin{minted}[autogobble, linenos, firstnumber = 10, tabsize = 4]{maxima}
			V: 140$
			I: rectform(V / Z_1234);
			rectform(linsolve(
				[
					I_1 + I_234 = I,
					I_1 * Z_1 = I_234 * Z_234
				],
				[I_1, I_234]
			));
			map(lambda([eq], lhs(eq) :: rhs(eq)), %)$
			I_2: I_234$
			rectform(linsolve(
				[
					I_3 + I_4 = I_234,
					I_3 * Z_3 = I_4 * Z_4
				],
				[I_3, I_4]
			));
			map(lambda([eq], lhs(eq) :: rhs(eq)), %)$
		\end{minted}
		Note that \maxinline{I_2: I_234} comes from the fact that the current passing through each of
		the impedances in a series is the same, thus $\text{\maxinline{I_2}} =
		\text{\maxinline{Z_234}}$.

		\homeworkPart
		Determine the real power absorbed by each impedance. \\
		
		The real power absorbed by each impedance is as follows:
		\begin{align*}
			P_{Z_1} &= \re{\abs{I_1}^2 \cdot Z_1}
				= \frac{98000}{89}\si{\watt}
					\approx \qty{1101.124}{\watt} \\
			P_{Z_2} &= \re{\abs{I_2}^2 \cdot Z_2}
				= \frac{133280}{169}\si{\watt}
					\approx \qty{788.630}{\watt} \\
			P_{Z_3} &= \re{\abs{I_3}^2 \cdot Z_3}
				= \frac{3920}{13}\si{\watt}
					\approx \qty{301.538}{\watt} \\
			P_{Z_4} &= \re{\abs{I_4}^2 \cdot Z_4}
				= \frac{23520}{169}\si{\watt}
					\approx \qty{139.171}{\watt}
		\end{align*}
		
		\homeworkPart
		Determine the reactive power of each impedance. Clearly state whether reactive power is
		delivered or absorbed by each impedance.
		
		The reactive power of each impedance is as follows:
		\begin{align*}
			Q_{Z_1} &= \im{\abs{I_1}^2 \cdot Z_1}
				= -\frac{156800}{89}\si{\var}
					\approx \qty{-1761.798}{\var}
				& \text{(delivered)} \\
			Q_{Z_2} &= \im{\abs{I_2}^2 \cdot Z_2}
				= \frac{66640}{169}\si{\var}
					\approx \qty{394.320}{\var}
				& \text{(absorbed)} \\
			Q_{Z_3} &= \im{\abs{I_3}^2 \cdot Z_3}
				= -\frac{3920}{13}\si{\var}
					\approx \qty{-301.538}{\var}
				& \text{(delivered)} \\
			Q_{Z_4} &= \im{\abs{I_4}^2 \cdot Z_4}
				= \frac{15680}{169}\si{\var}
					\approx \qty{92.781}{\var}
				& \text{(absorbed)}
		\end{align*}
	\end{homeworkProblem}
\end{document}
