# latex-homework-template

## Description
This is a homework template that I cooked up, heavily based on [this](https://github.com/jdavis/latex-homework-template) one someone pointed me to a while back. I used the same page setup and same `\begin{homeworkProblem}`/`\end{homeworkProblem}` environment, but heavily modified the internals of the environment to allow for changing the problem number format between `chapter.problem` and just `problem`. I've also added a `\homeworkPart` command and included some homework problems that I've done to give an idea of what a more complete document using it might look like.

## Features
Compared to the orignal template, this one allows for a little more flexibility in the homework problem environment and with the homework part command. For instance:
```latex
\begin{homeworkProblem}[chapterNumber = 10, problemNumber = 8, showChapterNumber = true]
    \homeworkPart[partNumber = 3, showPartAs = numer]
    foo bar baz baq
\end{homeworkProblem}
```
will create a section "Problem 10.8", and the first part within that is "Part 3". However, if you don't need chapter numbers, you can omit `chapterNumber = ...` and `showChapterNumber = true`:
```latex
\begin{homeworkProblem}[problemNumber = 8]
    \homeworkPart[partNumber = 3, showPartAs = numer]
    foo bar baz baq
\end{homeworkProblem}
```
And if you'd rather show the part as a letter instead of number, then leave out `showPartAs = numer`:
```latex
\begin{homeworkProblem}[problemNumber = 8]
    \homeworkPart[partNumber = 3]
    foo bar baz baq
\end{homeworkProblem}
```
And now the first part in the problem will be "Part C". The `homeworkProblem` environment increments a problem number counter (starting from 1) at its closing, so one may also do something to the effect of
```latex
\begin{homeworkProblem}
    This will be problem 1.
\end{homeworkProblem}
\begin{homeworkProblem}
    This will be problem 2.
\end{homeworkProblem}
```
Similarly, `\homeworkPart` increments a counter starting from 1 (which is rendered with `\Alph{partCounter}`), though an optional argument can specify which number to use instead, like the `\homeworkPart[3]` from above. There also exists `\nextChapter` and `\setChapter{number}` for changing the chapter number, since the chapter number is preserved between environment usages. Specifically:
```latex
\begin{homeworkProblem}[chapterNumber = 2, showChapterNumber = true]
    This will be problem 2.1.
\end{homeworkProblem}
\begin{homeworkProblem}[showChapterNumber = true]
    This will be problem 2.2.
\end{homeworkProblem}
```
Thus the purpose of those commands is just to be a shorthand for `\stepcounter{chapterCounter}\setcounter{problemCounter}{1}` and `\setcounter{chapterCounter}{number}\setcounter{problemCounter}{1}` respectively.

## Contributing
If I'm gonna be honest, I probably screwed up real bad somewhere I haven't found yet and something isn't formatted as I intended it to be. If you find such a screwup, I'd appreciate an issue/PR letting me know.

## Authors and acknowledgment
Shoutouts again to jdavis and [the original template this was based on](https://github.com/jdavis/latex-homework-template).

Also yes, some of the problems in the example file may have wrong answers - I copied stuff from homework I did this semester that had not yet been graded, and I'm not such an exceedingly clever person that I never get homework problems wrong.

## License
This project is licensed under the EUPL 1.1 license.
